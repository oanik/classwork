import React from "react";
import ReactDOM from "react-dom";
import App from "./components/app.js";

const data = [
	{ name: "React", href: "https://uk.reactjs.org/" },
	{ name: "Javascript", href: "https://learn.javascript.ru/" },
	{ name: "Jquery", href: "https://jquery.com/" },
];

//const data = ["About", "Home", "Contact"];
ReactDOM.render(<App data={data} />, document.querySelector("#root"));
