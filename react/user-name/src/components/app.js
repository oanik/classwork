import React from "react";
import logo from "../image/cover.jpg";
import "../components/styles.css";
import NaviBar from "../components/navBar.js";
import Text from "../components/textBlock.js";

const App = (props) => {
	debugger;
	return (
		<>
			<div className='main'>
				<ul>
					<NaviBar navBar={props.data} />
				</ul>
			</div>
			<img className='image' src={logo} alt='phone' />
			<div>
				<Text />
			</div>
		</>
	);
};

export default App;
