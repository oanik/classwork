import React from "react";
import "../components/styles.css";

const Text = () => {
	return (
		<>
			<h1>
				{
					" Choosing a Tech Stack for the Full-Cycle Web Application Development"
				}
			</h1>
			<p>
				"In 2019 one of the most important steps to take when it comes to
				developing a successful digital software product is to pick the right
				technology stack. Why? Because creating a product is not just about
				designing a nice UI and convenient UX; it’s also about designing a
				stable, secure, and maintainable product that will not only win your
				customer’s heart but will also allow you to scale your business. Here’s
				where the right technology may help. While you, as a business owner, are
				busy with things like elaborating your business idea, defining your
				product’s pricing model, and coming up with powerful marketing, deciding
				on technologies for your new app is something you'll likely leave up to
				your developers. Of course, it’s common practice to rely on your
				development partner’s technology suggestions. If you do, however, you
				should make sure your partner understands your business needs and takes
				into account all important features you’ll be implementing in order to
				choose suitable technologies. At Yalantis, we believe that having a
				general understanding of the web app technology stack is a must for a
				client. It helps us to speak the same technical language and effectively
				reach your goals. This is why we’ve prepared this article. Without
				further ado, let’s explore what a technology stack is and what tools we
				use at Yalantis to build your web products."
			</p>
			;
		</>
	);
};

export default Text;
