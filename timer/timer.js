const get = (id) => {
	return document.getElementById(id);
};

class Timer {
	constructor() {
		this.timerId = 0;
		this.seconds;
		this.minutes;
		this.hours;
		this.days;
		this.distance;
		this.countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();
		this.currentTime = new Date("Sep 5, 2020 15:37:25").getTime();
	}

	run() {
		// main logic to calculate time difference and count number of days, hours, min, sec
		this.distance = this.countDownDate - this.currentTime;
		// Time calculations for days, hours, minutes and seconds
		this.days = Math.floor(this.distance / (1000 * 60 * 60 * 24));
		this.hours = Math.floor(
			(this.distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
		);
		this.minutes = Math.floor((this.distance % (1000 * 60 * 60)) / (1000 * 60));
		this.seconds = Math.floor((this.distance % (1000 * 60)) / 1000);

		get(
			"info"
		).textContent = ` ${this.days}:${this.hours}:${this.minutes}:${this.seconds}`;
		this.currentTime += 1000;
		if (this.distance < 0) {
			clearInterval(this.timerId);
			get("info").textContent = "EXPIRED";
		}
	}
	start() {
		let self = this;
		this.timerId = setInterval(function () {
			self.run();
		}, 1000);
	}
	stop() {
		clearInterval(this.timerId);
	}
	reset() {
		this.stop();
		this.currentTime = new Date("Sep 5, 2020 15:37:25").getTime();
		this.start();
	}
}

const timer = new Timer();

window.onload = () => {
	get("init").onclick = function () {
		timer.start();
	};
	get("stop").onclick = function () {
		timer.stop();
	};
	get("restart").onclick = function () {
		timer.reset();
	};
};
