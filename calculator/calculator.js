//  метод получения объекта  DOM  по селектору
const get = (id) => {
	return document.getElementById(id);
};

class Calculator {
	constructor() {
		this.operandArr = [];
		this.operation;
		this.result;
		this.container = "";
		this.output = get("output");
		this.expressionPattern = /(0|[1-9]\d*)([.,]\d+)?\s*([\+\*\-\/])\s*(0|[1-9]\d*)([.,]\d+)?\=/;

		this.regExpNums = /[0-9]/;
		this.regExpOper = /[\-\(/)\.\*\+]/;
		this.regExpEquals = /\=/;
	}
	//слушает ивент, проверяет символ ввоба и добавляет в строку container
	input(event) {
		let target = event.target.value;
		this.newClick();
		console.log(target, typeof target);
		if (target == "AC") {
			alert("Clear ALL");
			this.clearOutput();
		} else if (this.regExpNums.test(target)) {
			this.container += target;
			this.showOutput(target);
		} else if (this.regExpOper.test(target)) {
			this.container += target;
			this.showOutput(target);
		} else if (this.regExpEquals.test(target)) {
			this.container += target;
			this.showOutput(target);
			this.checkMethod(this.container);
		} else {
			alert("Wrong Symbol.Try again");
		}
	}
	//    проверяет всю строку выражения в соответствии с регулярным выражением и запускает следующие методы если true
	checkMethod(expression) {
		if (this.expressionPattern.test(expression)) {
			this.parseExpression(expression);
			this.chooseMathOperation();
			this.showOutput(this.result);
		} else {
			alert("Wrong input");
			this.clearOutput();
		}
	}

	//   парсит выражение из строки  на операнды (числа и знаки) и присваивает значения мемберам -операндам
	parseExpression(exprss) {
		let operand = "";
		for (let index in exprss) {
			if (
				parseFloat(exprss[index]) ||
				exprss[index] == "0" ||
				exprss[index] == "."
			) {
				operand += exprss[index];
			} else {
				this.operandArr.push(parseFloat(operand, 10));
				operand = "";
				if (exprss[index] != "=") this.operation = exprss[index];
			}
		}
	}
	//  выбор математической операции
	chooseMathOperation() {
		switch (this.operation) {
			case "*":
				this.multiplay();
				break;
			case "/":
				this.divide();
				break;
			case "+":
				this.add();
				break;
			case "-":
				this.subtruct();
				break;
		}
	}
	//  При клике после получения результата проверяет результат, очищает значения мемберов и добавляет в строку контейнер значение предыдущего результата
	newClick() {
		let temp;
		if (this.result != null) {
			temp = this.result;
			this.clearOutput();
			this.showOutput(temp);
			this.container = temp;
		}
	}
	//   операция умножения с округлением до сотых
	multiplay() {
		let temp;
		this.result = this.operandArr[0] * this.operandArr[1];
		temp = this.result;
		this.result = parseFloat(temp.toFixed(2));
	}

	//   операция прибавления с округлением до сотых
	add() {
		let temp;
		this.result = this.operandArr[0] + this.operandArr[1];
		temp = this.result;
		this.result = parseFloat(temp.toFixed(2));
	}
	//   операция вычитания с округлением до соты
	subtruct() {
		let temp;
		this.result = this.operandArr[0] - this.operandArr[1];
		temp = this.result;
		this.result = parseFloat(temp.toFixed(2));
	}
	//   операция деления с округлением до сотых
	divide() {
		if (this.operandArr[1] == 0) {
			this.result = "Infinity";
			return;
		}
		this.result = this.operandArr[0] / this.operandArr[1];
		let temp = this.result;
		this.result = parseFloat(temp.toFixed(2));
	}
	//   очистка областим вывода и значений  некоторых мемберов класса
	clearOutput() {
		this.output = get("output");
		this.output.textContent = "";
		this.operandArr = [];
		this.result = null;
		this.operation = "";
		this.container = "";
	}
	//   показать вывод на экран
	showOutput(value) {
		this.output = get("output");

		let theCSSprop = window
			.getComputedStyle(this.output, null)
			.getPropertyValue("width");
		if (theCSSprop <= "208px") {
			this.output.textContent += value;
		} else {
			this.output.textContent += " ";
			this.output.textContent += value;
		}
	}
}

let calculator = new Calculator();

//  обработчик событий на объект класса  Calculator
get("calculator").addEventListener("click", function (e) {
	calculator.input(e);
});
